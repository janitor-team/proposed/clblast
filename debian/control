Source: clblast
Maintainer: Gard Spreemann <gspr@nonempty.org>
Section: math
Priority: optional
Standards-Version: 4.6.1.1
Build-Depends: cmake,
               debhelper-compat (= 13),
               libblas-dev,
               ocl-icd-opencl-dev | opencl-dev
Rules-Requires-Root: no
Homepage: https://cnugteren.github.io/clblast/clblast.html
Vcs-Browser: https://salsa.debian.org/gspr/clblast
Vcs-Git: https://salsa.debian.org/gspr/clblast.git -b debian/sid

Package: clblast-tests
Section: math
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Tests for CLBlast
 CLBlast is a modern, lightweight, performant and tunable OpenCL BLAS
 library written in C++11. It is designed to leverage the full
 performance potential of a wide variety of OpenCL devices from
 different vendors, including desktop and laptop GPUs, embedded GPUs,
 and other accelerators. CLBlast implements BLAS routines: basic
 linear algebra subprograms operating on vectors and matrices.
 .
 This package provides tests and benchmarks against other BLAS
 implementations.

Package: clblast-utils
Section: math
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Utilities for CLBlast
 CLBlast is a modern, lightweight, performant and tunable OpenCL BLAS
 library written in C++11. It is designed to leverage the full
 performance potential of a wide variety of OpenCL devices from
 different vendors, including desktop and laptop GPUs, embedded GPUs,
 and other accelerators. CLBlast implements BLAS routines: basic
 linear algebra subprograms operating on vectors and matrices.
 .
 This package provides utilities for tuning the implementation for
 specific hardware.

Package: libclblast1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Tuned OpenCL BLAS library
 CLBlast is a modern, lightweight, performant and tunable OpenCL BLAS
 library written in C++11. It is designed to leverage the full
 performance potential of a wide variety of OpenCL devices from
 different vendors, including desktop and laptop GPUs, embedded GPUs,
 and other accelerators. CLBlast implements BLAS routines: basic
 linear algebra subprograms operating on vectors and matrices.
 .
 This package provides the shared library.

Package: libclblast-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         libclblast1 (= ${binary:Version}),
         ocl-icd-opencl-dev | opencl-dev
Description: Tuned OpenCL BLAS library (development files)
 CLBlast is a modern, lightweight, performant and tunable OpenCL BLAS
 library written in C++11. It is designed to leverage the full
 performance potential of a wide variety of OpenCL devices from
 different vendors, including desktop and laptop GPUs, embedded GPUs,
 and other accelerators. CLBlast implements BLAS routines: basic
 linear algebra subprograms operating on vectors and matrices.
 .
 This package provides the static library and headers.
